.PHONY: all
all : original

production/original/TwT1/TwT1_L1_R1.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT1/TwT1_L2_R1.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT1/TwT1_L3_R1.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT1/TwT1_L4_R1.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT1/TwT1_L7_R1.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT1/TwT1_L8_R1.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT1/TwT1_L1_R2.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT1/TwT1_L2_R2.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT1/TwT1_L3_R2.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT1/TwT1_L4_R2.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT1/TwT1_L7_R2.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT1/TwT1_L8_R2.fastq.gz : ;
	mkdir -p production/original/TwT1
	cp $< $@

production/original/TwT2/TwT2_L3_R1.fastq.gz : ;
	mkdir -p production/original/TwT2
	cp $< $@

production/original/TwT2/TwT2_L4_R1.fastq.gz : ;
	mkdir -p production/original/TwT2
	cp $< $@

production/original/TwT2/TwT2_L5_R1.fastq.gz : ;
	mkdir -p production/original/TwT2
	cp $< $@

production/original/TwT2/TwT2_L3_R2.fastq.gz : ;
	mkdir -p production/original/TwT2
	cp $< $@

production/original/TwT2/TwT2_L4_R2.fastq.gz : ;
	mkdir -p production/original/TwT2
	cp $< $@

production/original/TwT2/TwT2_L5_R2.fastq.gz : ;
	mkdir -p production/original/TwT2
	cp $< $@

production/original/TwN1/TwN1_L5_R1.fastq.gz : ;
	mkdir -p production/original/TwN1
	cp $< $@

production/original/TwN1/TwN1_L6_R1.fastq.gz : ;
	mkdir -p production/original/TwN1
	cp $< $@

production/original/TwN1/TwN1_L7_R1.fastq.gz : ;
	mkdir -p production/original/TwN1
	cp $< $@

production/original/TwN1/TwN1_L8_R1.fastq.gz : ;
	mkdir -p production/original/TwN1
	cp $< $@

production/original/TwN1/TwN1_L5_R2.fastq.gz : ;
	mkdir -p production/original/TwN1
	cp $< $@

production/original/TwN1/TwN1_L6_R2.fastq.gz : ;
	mkdir -p production/original/TwN1
	cp $< $@

production/original/TwN1/TwN1_L7_R2.fastq.gz : ;
	mkdir -p production/original/TwN1
	cp $< $@

production/original/TwN1/TwN1_L8_R2.fastq.gz : ;
	mkdir -p production/original/TwN1
	cp $< $@

production/original/TwN2/TwN2_L6_R1.fastq.gz : ;
	mkdir -p production/original/TwN2
	cp $< $@

production/original/TwN2/TwN2_L7_R1.fastq.gz : ;
	mkdir -p production/original/TwN2
	cp $< $@

production/original/TwN2/TwN2_L8_R1.fastq.gz : ;
	mkdir -p production/original/TwN2
	cp $< $@

production/original/TwN2/TwN2_L6_R2.fastq.gz : ;
	mkdir -p production/original/TwN2
	cp $< $@

production/original/TwN2/TwN2_L7_R2.fastq.gz : ;
	mkdir -p production/original/TwN2
	cp $< $@

production/original/TwN2/TwN2_L8_R2.fastq.gz : ;
	mkdir -p production/original/TwN2
	cp $< $@

production/original/TwT3/TwT3_L3_R1.fastq.gz : ;
	mkdir -p production/original/TwT3
	cp $< $@

production/original/TwT3/TwT3_L4_R1.fastq.gz : ;
	mkdir -p production/original/TwT3
	cp $< $@

production/original/TwT3/TwT3_L5_R1.fastq.gz : ;
	mkdir -p production/original/TwT3
	cp $< $@

production/original/TwT3/TwT3_L3_R2.fastq.gz : ;
	mkdir -p production/original/TwT3
	cp $< $@

production/original/TwT3/TwT3_L4_R2.fastq.gz : ;
	mkdir -p production/original/TwT3
	cp $< $@

production/original/TwT3/TwT3_L5_R2.fastq.gz : ;
	mkdir -p production/original/TwT3
	cp $< $@

production/original/TwN3/TwN3_L6_R1.fastq.gz : ;
	mkdir -p production/original/TwN3
	cp $< $@

production/original/TwN3/TwN3_L7_R1.fastq.gz : ;
	mkdir -p production/original/TwN3
	cp $< $@

production/original/TwN3/TwN3_L8_R1.fastq.gz : ;
	mkdir -p production/original/TwN3
	cp $< $@

production/original/TwN3/TwN3_L6_R2.fastq.gz : ;
	mkdir -p production/original/TwN3
	cp $< $@

production/original/TwN3/TwN3_L7_R2.fastq.gz : ;
	mkdir -p production/original/TwN3
	cp $< $@

production/original/TwN3/TwN3_L8_R2.fastq.gz : ;
	mkdir -p production/original/TwN3
	cp $< $@

production/original/U1T/U1T_L2_R1.fastq.gz : ;
	mkdir -p production/original/U1T
	cp $< $@

production/original/U1T/U1T_L3_R1.fastq.gz : ;
	mkdir -p production/original/U1T
	cp $< $@

production/original/U1T/U1T_L4_R1.fastq.gz : ;
	mkdir -p production/original/U1T
	cp $< $@

production/original/U1T/U1T_L2_R2.fastq.gz : ;
	mkdir -p production/original/U1T
	cp $< $@

production/original/U1T/U1T_L3_R2.fastq.gz : ;
	mkdir -p production/original/U1T
	cp $< $@

production/original/U1T/U1T_L4_R2.fastq.gz : ;
	mkdir -p production/original/U1T
	cp $< $@

production/original/U2T/U2T_L1_R1.fastq.gz : ;
	mkdir -p production/original/U2T
	cp $< $@

production/original/U2T/U2T_L2_R1.fastq.gz : ;
	mkdir -p production/original/U2T
	cp $< $@

production/original/U2T/U2T_L3_R1.fastq.gz : ;
	mkdir -p production/original/U2T
	cp $< $@

production/original/U2T/U2T_L1_R2.fastq.gz : ;
	mkdir -p production/original/U2T
	cp $< $@

production/original/U2T/U2T_L2_R2.fastq.gz : ;
	mkdir -p production/original/U2T
	cp $< $@

production/original/U2T/U2T_L3_R2.fastq.gz : ;
	mkdir -p production/original/U2T
	cp $< $@

production/original/U3N/U3N_L4_R1.fastq.gz : ;
	mkdir -p production/original/U3N
	cp $< $@

production/original/U3N/U3N_L5_R1.fastq.gz : ;
	mkdir -p production/original/U3N
	cp $< $@

production/original/U3N/U3N_L6_R1.fastq.gz : ;
	mkdir -p production/original/U3N
	cp $< $@

production/original/U3N/U3N_L4_R2.fastq.gz : ;
	mkdir -p production/original/U3N
	cp $< $@

production/original/U3N/U3N_L5_R2.fastq.gz : ;
	mkdir -p production/original/U3N
	cp $< $@

production/original/U3N/U3N_L6_R2.fastq.gz : ;
	mkdir -p production/original/U3N
	cp $< $@

production/original/U4T/U4T_L1_R1.fastq.gz : ;
	mkdir -p production/original/U4T
	cp $< $@

production/original/U4T/U4T_L7_R1.fastq.gz : ;
	mkdir -p production/original/U4T
	cp $< $@

production/original/U4T/U4T_L8_R1.fastq.gz : ;
	mkdir -p production/original/U4T
	cp $< $@

production/original/U4T/U4T_L1_R2.fastq.gz : ;
	mkdir -p production/original/U4T
	cp $< $@

production/original/U4T/U4T_L7_R2.fastq.gz : ;
	mkdir -p production/original/U4T
	cp $< $@

production/original/U4T/U4T_L8_R2.fastq.gz : ;
	mkdir -p production/original/U4T
	cp $< $@

production/combined/TwT1/TwT1_R1.fastq.gz : production/original/TwT1/TwT1_L1_R1.fastq.gz production/original/TwT1/TwT1_L2_R1.fastq.gz production/original/TwT1/TwT1_L3_R1.fastq.gz production/original/TwT1/TwT1_L4_R1.fastq.gz production/original/TwT1/TwT1_L7_R1.fastq.gz production/original/TwT1/TwT1_L8_R1.fastq.gz
	mkdir -p production/combined/TwT1
	-(pigz -dc $^) | (pigz >production/combined/TwT1/TwT1_R1.fastq.gz)

production/combined/TwT1/TwT1_R2.fastq.gz : production/original/TwT1/TwT1_L1_R2.fastq.gz production/original/TwT1/TwT1_L2_R2.fastq.gz production/original/TwT1/TwT1_L3_R2.fastq.gz production/original/TwT1/TwT1_L4_R2.fastq.gz production/original/TwT1/TwT1_L7_R2.fastq.gz production/original/TwT1/TwT1_L8_R2.fastq.gz
	mkdir -p production/combined/TwT1
	-(pigz -dc $^) | (pigz >production/combined/TwT1/TwT1_R2.fastq.gz)

production/combined/TwT2/TwT2_R1.fastq.gz : production/original/TwT2/TwT2_L3_R1.fastq.gz production/original/TwT2/TwT2_L4_R1.fastq.gz production/original/TwT2/TwT2_L5_R1.fastq.gz
	mkdir -p production/combined/TwT2
	-(pigz -dc $^) | (pigz >production/combined/TwT2/TwT2_R1.fastq.gz)

production/combined/TwT2/TwT2_R2.fastq.gz : production/original/TwT2/TwT2_L3_R2.fastq.gz production/original/TwT2/TwT2_L4_R2.fastq.gz production/original/TwT2/TwT2_L5_R2.fastq.gz
	mkdir -p production/combined/TwT2
	-(pigz -dc $^) | (pigz >production/combined/TwT2/TwT2_R2.fastq.gz)

production/combined/TwN1/TwN1_R1.fastq.gz : production/original/TwN1/TwN1_L5_R1.fastq.gz production/original/TwN1/TwN1_L6_R1.fastq.gz production/original/TwN1/TwN1_L7_R1.fastq.gz production/original/TwN1/TwN1_L8_R1.fastq.gz
	mkdir -p production/combined/TwN1
	-(pigz -dc $^) | (pigz >production/combined/TwN1/TwN1_R1.fastq.gz)

production/combined/TwN1/TwN1_R2.fastq.gz : production/original/TwN1/TwN1_L5_R2.fastq.gz production/original/TwN1/TwN1_L6_R2.fastq.gz production/original/TwN1/TwN1_L7_R2.fastq.gz production/original/TwN1/TwN1_L8_R2.fastq.gz
	mkdir -p production/combined/TwN1
	-(pigz -dc $^) | (pigz >production/combined/TwN1/TwN1_R2.fastq.gz)

production/combined/TwN2/TwN2_R1.fastq.gz : production/original/TwN2/TwN2_L6_R1.fastq.gz production/original/TwN2/TwN2_L7_R1.fastq.gz production/original/TwN2/TwN2_L8_R1.fastq.gz
	mkdir -p production/combined/TwN2
	-(pigz -dc $^) | (pigz >production/combined/TwN2/TwN2_R1.fastq.gz)

production/combined/TwN2/TwN2_R2.fastq.gz : production/original/TwN2/TwN2_L6_R2.fastq.gz production/original/TwN2/TwN2_L7_R2.fastq.gz production/original/TwN2/TwN2_L8_R2.fastq.gz
	mkdir -p production/combined/TwN2
	-(pigz -dc $^) | (pigz >production/combined/TwN2/TwN2_R2.fastq.gz)

production/combined/TwT3/TwT3_R1.fastq.gz : production/original/TwT3/TwT3_L3_R1.fastq.gz production/original/TwT3/TwT3_L4_R1.fastq.gz production/original/TwT3/TwT3_L5_R1.fastq.gz
	mkdir -p production/combined/TwT3
	-(pigz -dc $^) | (pigz >production/combined/TwT3/TwT3_R1.fastq.gz)

production/combined/TwT3/TwT3_R2.fastq.gz : production/original/TwT3/TwT3_L3_R2.fastq.gz production/original/TwT3/TwT3_L4_R2.fastq.gz production/original/TwT3/TwT3_L5_R2.fastq.gz
	mkdir -p production/combined/TwT3
	-(pigz -dc $^) | (pigz >production/combined/TwT3/TwT3_R2.fastq.gz)

production/combined/TwN3/TwN3_R1.fastq.gz : production/original/TwN3/TwN3_L6_R1.fastq.gz production/original/TwN3/TwN3_L7_R1.fastq.gz production/original/TwN3/TwN3_L8_R1.fastq.gz
	mkdir -p production/combined/TwN3
	-(pigz -dc $^) | (pigz >production/combined/TwN3/TwN3_R1.fastq.gz)

production/combined/TwN3/TwN3_R2.fastq.gz : production/original/TwN3/TwN3_L6_R2.fastq.gz production/original/TwN3/TwN3_L7_R2.fastq.gz production/original/TwN3/TwN3_L8_R2.fastq.gz
	mkdir -p production/combined/TwN3
	-(pigz -dc $^) | (pigz >production/combined/TwN3/TwN3_R2.fastq.gz)

production/combined/U1T/U1T_R1.fastq.gz : production/original/U1T/U1T_L2_R1.fastq.gz production/original/U1T/U1T_L3_R1.fastq.gz production/original/U1T/U1T_L4_R1.fastq.gz
	mkdir -p production/combined/U1T
	-(pigz -dc $^) | (pigz >production/combined/U1T/U1T_R1.fastq.gz)

production/combined/U1T/U1T_R2.fastq.gz : production/original/U1T/U1T_L2_R2.fastq.gz production/original/U1T/U1T_L3_R2.fastq.gz production/original/U1T/U1T_L4_R2.fastq.gz
	mkdir -p production/combined/U1T
	-(pigz -dc $^) | (pigz >production/combined/U1T/U1T_R2.fastq.gz)

production/combined/U2T/U2T_R1.fastq.gz : production/original/U2T/U2T_L1_R1.fastq.gz production/original/U2T/U2T_L2_R1.fastq.gz production/original/U2T/U2T_L3_R1.fastq.gz
	mkdir -p production/combined/U2T
	-(pigz -dc $^) | (pigz >production/combined/U2T/U2T_R1.fastq.gz)

production/combined/U2T/U2T_R2.fastq.gz : production/original/U2T/U2T_L1_R2.fastq.gz production/original/U2T/U2T_L2_R2.fastq.gz production/original/U2T/U2T_L3_R2.fastq.gz
	mkdir -p production/combined/U2T
	-(pigz -dc $^) | (pigz >production/combined/U2T/U2T_R2.fastq.gz)

production/combined/U3N/U3N_R1.fastq.gz : production/original/U3N/U3N_L4_R1.fastq.gz production/original/U3N/U3N_L5_R1.fastq.gz production/original/U3N/U3N_L6_R1.fastq.gz
	mkdir -p production/combined/U3N
	-(pigz -dc $^) | (pigz >production/combined/U3N/U3N_R1.fastq.gz)

production/combined/U3N/U3N_R2.fastq.gz : production/original/U3N/U3N_L4_R2.fastq.gz production/original/U3N/U3N_L5_R2.fastq.gz production/original/U3N/U3N_L6_R2.fastq.gz
	mkdir -p production/combined/U3N
	-(pigz -dc $^) | (pigz >production/combined/U3N/U3N_R2.fastq.gz)

production/combined/U4T/U4T_R1.fastq.gz : production/original/U4T/U4T_L1_R1.fastq.gz production/original/U4T/U4T_L7_R1.fastq.gz production/original/U4T/U4T_L8_R1.fastq.gz
	mkdir -p production/combined/U4T
	-(pigz -dc $^) | (pigz >production/combined/U4T/U4T_R1.fastq.gz)

production/combined/U4T/U4T_R2.fastq.gz : production/original/U4T/U4T_L1_R2.fastq.gz production/original/U4T/U4T_L7_R2.fastq.gz production/original/U4T/U4T_L8_R2.fastq.gz
	mkdir -p production/combined/U4T
	-(pigz -dc $^) | (pigz >production/combined/U4T/U4T_R2.fastq.gz)

truncated/original/TwT1/TwT1_R1_L1.fastq.gz : production/original/TwT1/TwT1_L1_R1.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT1/TwT1_R1_L2.fastq.gz : production/original/TwT1/TwT1_L2_R1.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT1/TwT1_R1_L3.fastq.gz : production/original/TwT1/TwT1_L3_R1.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT1/TwT1_R1_L4.fastq.gz : production/original/TwT1/TwT1_L4_R1.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT1/TwT1_R1_L7.fastq.gz : production/original/TwT1/TwT1_L7_R1.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT1/TwT1_R1_L8.fastq.gz : production/original/TwT1/TwT1_L8_R1.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT1/TwT1_R2_L1.fastq.gz : production/original/TwT1/TwT1_L1_R2.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT1/TwT1_R2_L2.fastq.gz : production/original/TwT1/TwT1_L2_R2.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT1/TwT1_R2_L3.fastq.gz : production/original/TwT1/TwT1_L3_R2.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT1/TwT1_R2_L4.fastq.gz : production/original/TwT1/TwT1_L4_R2.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT1/TwT1_R2_L7.fastq.gz : production/original/TwT1/TwT1_L7_R2.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT1/TwT1_R2_L8.fastq.gz : production/original/TwT1/TwT1_L8_R2.fastq.gz
	mkdir -p truncated/original/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT2/TwT2_R1_L3.fastq.gz : production/original/TwT2/TwT2_L3_R1.fastq.gz
	mkdir -p truncated/original/TwT2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT2/TwT2_R1_L4.fastq.gz : production/original/TwT2/TwT2_L4_R1.fastq.gz
	mkdir -p truncated/original/TwT2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT2/TwT2_R1_L5.fastq.gz : production/original/TwT2/TwT2_L5_R1.fastq.gz
	mkdir -p truncated/original/TwT2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT2/TwT2_R2_L3.fastq.gz : production/original/TwT2/TwT2_L3_R2.fastq.gz
	mkdir -p truncated/original/TwT2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT2/TwT2_R2_L4.fastq.gz : production/original/TwT2/TwT2_L4_R2.fastq.gz
	mkdir -p truncated/original/TwT2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT2/TwT2_R2_L5.fastq.gz : production/original/TwT2/TwT2_L5_R2.fastq.gz
	mkdir -p truncated/original/TwT2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN1/TwN1_R1_L5.fastq.gz : production/original/TwN1/TwN1_L5_R1.fastq.gz
	mkdir -p truncated/original/TwN1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN1/TwN1_R1_L6.fastq.gz : production/original/TwN1/TwN1_L6_R1.fastq.gz
	mkdir -p truncated/original/TwN1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN1/TwN1_R1_L7.fastq.gz : production/original/TwN1/TwN1_L7_R1.fastq.gz
	mkdir -p truncated/original/TwN1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN1/TwN1_R1_L8.fastq.gz : production/original/TwN1/TwN1_L8_R1.fastq.gz
	mkdir -p truncated/original/TwN1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN1/TwN1_R2_L5.fastq.gz : production/original/TwN1/TwN1_L5_R2.fastq.gz
	mkdir -p truncated/original/TwN1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN1/TwN1_R2_L6.fastq.gz : production/original/TwN1/TwN1_L6_R2.fastq.gz
	mkdir -p truncated/original/TwN1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN1/TwN1_R2_L7.fastq.gz : production/original/TwN1/TwN1_L7_R2.fastq.gz
	mkdir -p truncated/original/TwN1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN1/TwN1_R2_L8.fastq.gz : production/original/TwN1/TwN1_L8_R2.fastq.gz
	mkdir -p truncated/original/TwN1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN2/TwN2_R1_L6.fastq.gz : production/original/TwN2/TwN2_L6_R1.fastq.gz
	mkdir -p truncated/original/TwN2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN2/TwN2_R1_L7.fastq.gz : production/original/TwN2/TwN2_L7_R1.fastq.gz
	mkdir -p truncated/original/TwN2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN2/TwN2_R1_L8.fastq.gz : production/original/TwN2/TwN2_L8_R1.fastq.gz
	mkdir -p truncated/original/TwN2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN2/TwN2_R2_L6.fastq.gz : production/original/TwN2/TwN2_L6_R2.fastq.gz
	mkdir -p truncated/original/TwN2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN2/TwN2_R2_L7.fastq.gz : production/original/TwN2/TwN2_L7_R2.fastq.gz
	mkdir -p truncated/original/TwN2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN2/TwN2_R2_L8.fastq.gz : production/original/TwN2/TwN2_L8_R2.fastq.gz
	mkdir -p truncated/original/TwN2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT3/TwT3_R1_L3.fastq.gz : production/original/TwT3/TwT3_L3_R1.fastq.gz
	mkdir -p truncated/original/TwT3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT3/TwT3_R1_L4.fastq.gz : production/original/TwT3/TwT3_L4_R1.fastq.gz
	mkdir -p truncated/original/TwT3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT3/TwT3_R1_L5.fastq.gz : production/original/TwT3/TwT3_L5_R1.fastq.gz
	mkdir -p truncated/original/TwT3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT3/TwT3_R2_L3.fastq.gz : production/original/TwT3/TwT3_L3_R2.fastq.gz
	mkdir -p truncated/original/TwT3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT3/TwT3_R2_L4.fastq.gz : production/original/TwT3/TwT3_L4_R2.fastq.gz
	mkdir -p truncated/original/TwT3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwT3/TwT3_R2_L5.fastq.gz : production/original/TwT3/TwT3_L5_R2.fastq.gz
	mkdir -p truncated/original/TwT3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN3/TwN3_R1_L6.fastq.gz : production/original/TwN3/TwN3_L6_R1.fastq.gz
	mkdir -p truncated/original/TwN3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN3/TwN3_R1_L7.fastq.gz : production/original/TwN3/TwN3_L7_R1.fastq.gz
	mkdir -p truncated/original/TwN3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN3/TwN3_R1_L8.fastq.gz : production/original/TwN3/TwN3_L8_R1.fastq.gz
	mkdir -p truncated/original/TwN3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN3/TwN3_R2_L6.fastq.gz : production/original/TwN3/TwN3_L6_R2.fastq.gz
	mkdir -p truncated/original/TwN3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN3/TwN3_R2_L7.fastq.gz : production/original/TwN3/TwN3_L7_R2.fastq.gz
	mkdir -p truncated/original/TwN3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/TwN3/TwN3_R2_L8.fastq.gz : production/original/TwN3/TwN3_L8_R2.fastq.gz
	mkdir -p truncated/original/TwN3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U1T/U1T_R1_L2.fastq.gz : production/original/U1T/U1T_L2_R1.fastq.gz
	mkdir -p truncated/original/U1T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U1T/U1T_R1_L3.fastq.gz : production/original/U1T/U1T_L3_R1.fastq.gz
	mkdir -p truncated/original/U1T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U1T/U1T_R1_L4.fastq.gz : production/original/U1T/U1T_L4_R1.fastq.gz
	mkdir -p truncated/original/U1T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U1T/U1T_R2_L2.fastq.gz : production/original/U1T/U1T_L2_R2.fastq.gz
	mkdir -p truncated/original/U1T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U1T/U1T_R2_L3.fastq.gz : production/original/U1T/U1T_L3_R2.fastq.gz
	mkdir -p truncated/original/U1T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U1T/U1T_R2_L4.fastq.gz : production/original/U1T/U1T_L4_R2.fastq.gz
	mkdir -p truncated/original/U1T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U2T/U2T_R1_L1.fastq.gz : production/original/U2T/U2T_L1_R1.fastq.gz
	mkdir -p truncated/original/U2T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U2T/U2T_R1_L2.fastq.gz : production/original/U2T/U2T_L2_R1.fastq.gz
	mkdir -p truncated/original/U2T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U2T/U2T_R1_L3.fastq.gz : production/original/U2T/U2T_L3_R1.fastq.gz
	mkdir -p truncated/original/U2T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U2T/U2T_R2_L1.fastq.gz : production/original/U2T/U2T_L1_R2.fastq.gz
	mkdir -p truncated/original/U2T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U2T/U2T_R2_L2.fastq.gz : production/original/U2T/U2T_L2_R2.fastq.gz
	mkdir -p truncated/original/U2T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U2T/U2T_R2_L3.fastq.gz : production/original/U2T/U2T_L3_R2.fastq.gz
	mkdir -p truncated/original/U2T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U3N/U3N_R1_L4.fastq.gz : production/original/U3N/U3N_L4_R1.fastq.gz
	mkdir -p truncated/original/U3N
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U3N/U3N_R1_L5.fastq.gz : production/original/U3N/U3N_L5_R1.fastq.gz
	mkdir -p truncated/original/U3N
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U3N/U3N_R1_L6.fastq.gz : production/original/U3N/U3N_L6_R1.fastq.gz
	mkdir -p truncated/original/U3N
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U3N/U3N_R2_L4.fastq.gz : production/original/U3N/U3N_L4_R2.fastq.gz
	mkdir -p truncated/original/U3N
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U3N/U3N_R2_L5.fastq.gz : production/original/U3N/U3N_L5_R2.fastq.gz
	mkdir -p truncated/original/U3N
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U3N/U3N_R2_L6.fastq.gz : production/original/U3N/U3N_L6_R2.fastq.gz
	mkdir -p truncated/original/U3N
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U4T/U4T_R1_L1.fastq.gz : production/original/U4T/U4T_L1_R1.fastq.gz
	mkdir -p truncated/original/U4T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U4T/U4T_R1_L7.fastq.gz : production/original/U4T/U4T_L7_R1.fastq.gz
	mkdir -p truncated/original/U4T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U4T/U4T_R1_L8.fastq.gz : production/original/U4T/U4T_L8_R1.fastq.gz
	mkdir -p truncated/original/U4T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U4T/U4T_R2_L1.fastq.gz : production/original/U4T/U4T_L1_R2.fastq.gz
	mkdir -p truncated/original/U4T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U4T/U4T_R2_L7.fastq.gz : production/original/U4T/U4T_L7_R2.fastq.gz
	mkdir -p truncated/original/U4T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/original/U4T/U4T_R2_L8.fastq.gz : production/original/U4T/U4T_L8_R2.fastq.gz
	mkdir -p truncated/original/U4T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwT1/TwT1_R1.fastq.gz : production/combined/TwT1/TwT1_R1.fastq.gz
	mkdir -p truncated/combined/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwT1/TwT1_R2.fastq.gz : production/combined/TwT1/TwT1_R2.fastq.gz
	mkdir -p truncated/combined/TwT1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwT2/TwT2_R1.fastq.gz : production/combined/TwT2/TwT2_R1.fastq.gz
	mkdir -p truncated/combined/TwT2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwT2/TwT2_R2.fastq.gz : production/combined/TwT2/TwT2_R2.fastq.gz
	mkdir -p truncated/combined/TwT2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwN1/TwN1_R1.fastq.gz : production/combined/TwN1/TwN1_R1.fastq.gz
	mkdir -p truncated/combined/TwN1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwN1/TwN1_R2.fastq.gz : production/combined/TwN1/TwN1_R2.fastq.gz
	mkdir -p truncated/combined/TwN1
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwN2/TwN2_R1.fastq.gz : production/combined/TwN2/TwN2_R1.fastq.gz
	mkdir -p truncated/combined/TwN2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwN2/TwN2_R2.fastq.gz : production/combined/TwN2/TwN2_R2.fastq.gz
	mkdir -p truncated/combined/TwN2
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwT3/TwT3_R1.fastq.gz : production/combined/TwT3/TwT3_R1.fastq.gz
	mkdir -p truncated/combined/TwT3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwT3/TwT3_R2.fastq.gz : production/combined/TwT3/TwT3_R2.fastq.gz
	mkdir -p truncated/combined/TwT3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwN3/TwN3_R1.fastq.gz : production/combined/TwN3/TwN3_R1.fastq.gz
	mkdir -p truncated/combined/TwN3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/TwN3/TwN3_R2.fastq.gz : production/combined/TwN3/TwN3_R2.fastq.gz
	mkdir -p truncated/combined/TwN3
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/U1T/U1T_R1.fastq.gz : production/combined/U1T/U1T_R1.fastq.gz
	mkdir -p truncated/combined/U1T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/U1T/U1T_R2.fastq.gz : production/combined/U1T/U1T_R2.fastq.gz
	mkdir -p truncated/combined/U1T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/U2T/U2T_R1.fastq.gz : production/combined/U2T/U2T_R1.fastq.gz
	mkdir -p truncated/combined/U2T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/U2T/U2T_R2.fastq.gz : production/combined/U2T/U2T_R2.fastq.gz
	mkdir -p truncated/combined/U2T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/U3N/U3N_R1.fastq.gz : production/combined/U3N/U3N_R1.fastq.gz
	mkdir -p truncated/combined/U3N
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/U3N/U3N_R2.fastq.gz : production/combined/U3N/U3N_R2.fastq.gz
	mkdir -p truncated/combined/U3N
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/U4T/U4T_R1.fastq.gz : production/combined/U4T/U4T_R1.fastq.gz
	mkdir -p truncated/combined/U4T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

truncated/combined/U4T/U4T_R2.fastq.gz : production/combined/U4T/U4T_R2.fastq.gz
	mkdir -p truncated/combined/U4T
	-(pigz -dc $<) | (head -n 1000000) | (pigz >$@)

.PHONY: original
original : production/original/TwT1/TwT1_L1_R1.fastq.gz production/original/TwT1/TwT1_L2_R1.fastq.gz production/original/TwT1/TwT1_L3_R1.fastq.gz production/original/TwT1/TwT1_L4_R1.fastq.gz production/original/TwT1/TwT1_L7_R1.fastq.gz production/original/TwT1/TwT1_L8_R1.fastq.gz production/original/TwT1/TwT1_L1_R2.fastq.gz production/original/TwT1/TwT1_L2_R2.fastq.gz production/original/TwT1/TwT1_L3_R2.fastq.gz production/original/TwT1/TwT1_L4_R2.fastq.gz production/original/TwT1/TwT1_L7_R2.fastq.gz production/original/TwT1/TwT1_L8_R2.fastq.gz production/original/TwT2/TwT2_L3_R1.fastq.gz production/original/TwT2/TwT2_L4_R1.fastq.gz production/original/TwT2/TwT2_L5_R1.fastq.gz production/original/TwT2/TwT2_L3_R2.fastq.gz production/original/TwT2/TwT2_L4_R2.fastq.gz production/original/TwT2/TwT2_L5_R2.fastq.gz production/original/TwN1/TwN1_L5_R1.fastq.gz production/original/TwN1/TwN1_L6_R1.fastq.gz production/original/TwN1/TwN1_L7_R1.fastq.gz production/original/TwN1/TwN1_L8_R1.fastq.gz production/original/TwN1/TwN1_L5_R2.fastq.gz production/original/TwN1/TwN1_L6_R2.fastq.gz production/original/TwN1/TwN1_L7_R2.fastq.gz production/original/TwN1/TwN1_L8_R2.fastq.gz production/original/TwN2/TwN2_L6_R1.fastq.gz production/original/TwN2/TwN2_L7_R1.fastq.gz production/original/TwN2/TwN2_L8_R1.fastq.gz production/original/TwN2/TwN2_L6_R2.fastq.gz production/original/TwN2/TwN2_L7_R2.fastq.gz production/original/TwN2/TwN2_L8_R2.fastq.gz production/original/TwT3/TwT3_L3_R1.fastq.gz production/original/TwT3/TwT3_L4_R1.fastq.gz production/original/TwT3/TwT3_L5_R1.fastq.gz production/original/TwT3/TwT3_L3_R2.fastq.gz production/original/TwT3/TwT3_L4_R2.fastq.gz production/original/TwT3/TwT3_L5_R2.fastq.gz production/original/TwN3/TwN3_L6_R1.fastq.gz production/original/TwN3/TwN3_L7_R1.fastq.gz production/original/TwN3/TwN3_L8_R1.fastq.gz production/original/TwN3/TwN3_L6_R2.fastq.gz production/original/TwN3/TwN3_L7_R2.fastq.gz production/original/TwN3/TwN3_L8_R2.fastq.gz production/original/U1T/U1T_L2_R1.fastq.gz production/original/U1T/U1T_L3_R1.fastq.gz production/original/U1T/U1T_L4_R1.fastq.gz production/original/U1T/U1T_L2_R2.fastq.gz production/original/U1T/U1T_L3_R2.fastq.gz production/original/U1T/U1T_L4_R2.fastq.gz production/original/U2T/U2T_L1_R1.fastq.gz production/original/U2T/U2T_L2_R1.fastq.gz production/original/U2T/U2T_L3_R1.fastq.gz production/original/U2T/U2T_L1_R2.fastq.gz production/original/U2T/U2T_L2_R2.fastq.gz production/original/U2T/U2T_L3_R2.fastq.gz production/original/U3N/U3N_L4_R1.fastq.gz production/original/U3N/U3N_L5_R1.fastq.gz production/original/U3N/U3N_L6_R1.fastq.gz production/original/U3N/U3N_L4_R2.fastq.gz production/original/U3N/U3N_L5_R2.fastq.gz production/original/U3N/U3N_L6_R2.fastq.gz production/original/U4T/U4T_L1_R1.fastq.gz production/original/U4T/U4T_L7_R1.fastq.gz production/original/U4T/U4T_L8_R1.fastq.gz production/original/U4T/U4T_L1_R2.fastq.gz production/original/U4T/U4T_L7_R2.fastq.gz production/original/U4T/U4T_L8_R2.fastq.gz

.PHONY: combined
combined : production/combined/TwT1/TwT1_R1.fastq.gz production/combined/TwT1/TwT1_R2.fastq.gz production/combined/TwT2/TwT2_R1.fastq.gz production/combined/TwT2/TwT2_R2.fastq.gz production/combined/TwN1/TwN1_R1.fastq.gz production/combined/TwN1/TwN1_R2.fastq.gz production/combined/TwN2/TwN2_R1.fastq.gz production/combined/TwN2/TwN2_R2.fastq.gz production/combined/TwT3/TwT3_R1.fastq.gz production/combined/TwT3/TwT3_R2.fastq.gz production/combined/TwN3/TwN3_R1.fastq.gz production/combined/TwN3/TwN3_R2.fastq.gz production/combined/U1T/U1T_R1.fastq.gz production/combined/U1T/U1T_R2.fastq.gz production/combined/U2T/U2T_R1.fastq.gz production/combined/U2T/U2T_R2.fastq.gz production/combined/U3N/U3N_R1.fastq.gz production/combined/U3N/U3N_R2.fastq.gz production/combined/U4T/U4T_R1.fastq.gz production/combined/U4T/U4T_R2.fastq.gz

.PHONY: truncated_original
truncated_original : truncated/original/TwT1/TwT1_R1_L1.fastq.gz truncated/original/TwT1/TwT1_R1_L2.fastq.gz truncated/original/TwT1/TwT1_R1_L3.fastq.gz truncated/original/TwT1/TwT1_R1_L4.fastq.gz truncated/original/TwT1/TwT1_R1_L7.fastq.gz truncated/original/TwT1/TwT1_R1_L8.fastq.gz truncated/original/TwT1/TwT1_R2_L1.fastq.gz truncated/original/TwT1/TwT1_R2_L2.fastq.gz truncated/original/TwT1/TwT1_R2_L3.fastq.gz truncated/original/TwT1/TwT1_R2_L4.fastq.gz truncated/original/TwT1/TwT1_R2_L7.fastq.gz truncated/original/TwT1/TwT1_R2_L8.fastq.gz truncated/original/TwT2/TwT2_R1_L3.fastq.gz truncated/original/TwT2/TwT2_R1_L4.fastq.gz truncated/original/TwT2/TwT2_R1_L5.fastq.gz truncated/original/TwT2/TwT2_R2_L3.fastq.gz truncated/original/TwT2/TwT2_R2_L4.fastq.gz truncated/original/TwT2/TwT2_R2_L5.fastq.gz truncated/original/TwN1/TwN1_R1_L5.fastq.gz truncated/original/TwN1/TwN1_R1_L6.fastq.gz truncated/original/TwN1/TwN1_R1_L7.fastq.gz truncated/original/TwN1/TwN1_R1_L8.fastq.gz truncated/original/TwN1/TwN1_R2_L5.fastq.gz truncated/original/TwN1/TwN1_R2_L6.fastq.gz truncated/original/TwN1/TwN1_R2_L7.fastq.gz truncated/original/TwN1/TwN1_R2_L8.fastq.gz truncated/original/TwN2/TwN2_R1_L6.fastq.gz truncated/original/TwN2/TwN2_R1_L7.fastq.gz truncated/original/TwN2/TwN2_R1_L8.fastq.gz truncated/original/TwN2/TwN2_R2_L6.fastq.gz truncated/original/TwN2/TwN2_R2_L7.fastq.gz truncated/original/TwN2/TwN2_R2_L8.fastq.gz truncated/original/TwT3/TwT3_R1_L3.fastq.gz truncated/original/TwT3/TwT3_R1_L4.fastq.gz truncated/original/TwT3/TwT3_R1_L5.fastq.gz truncated/original/TwT3/TwT3_R2_L3.fastq.gz truncated/original/TwT3/TwT3_R2_L4.fastq.gz truncated/original/TwT3/TwT3_R2_L5.fastq.gz truncated/original/TwN3/TwN3_R1_L6.fastq.gz truncated/original/TwN3/TwN3_R1_L7.fastq.gz truncated/original/TwN3/TwN3_R1_L8.fastq.gz truncated/original/TwN3/TwN3_R2_L6.fastq.gz truncated/original/TwN3/TwN3_R2_L7.fastq.gz truncated/original/TwN3/TwN3_R2_L8.fastq.gz truncated/original/U1T/U1T_R1_L2.fastq.gz truncated/original/U1T/U1T_R1_L3.fastq.gz truncated/original/U1T/U1T_R1_L4.fastq.gz truncated/original/U1T/U1T_R2_L2.fastq.gz truncated/original/U1T/U1T_R2_L3.fastq.gz truncated/original/U1T/U1T_R2_L4.fastq.gz truncated/original/U2T/U2T_R1_L1.fastq.gz truncated/original/U2T/U2T_R1_L2.fastq.gz truncated/original/U2T/U2T_R1_L3.fastq.gz truncated/original/U2T/U2T_R2_L1.fastq.gz truncated/original/U2T/U2T_R2_L2.fastq.gz truncated/original/U2T/U2T_R2_L3.fastq.gz truncated/original/U3N/U3N_R1_L4.fastq.gz truncated/original/U3N/U3N_R1_L5.fastq.gz truncated/original/U3N/U3N_R1_L6.fastq.gz truncated/original/U3N/U3N_R2_L4.fastq.gz truncated/original/U3N/U3N_R2_L5.fastq.gz truncated/original/U3N/U3N_R2_L6.fastq.gz truncated/original/U4T/U4T_R1_L1.fastq.gz truncated/original/U4T/U4T_R1_L7.fastq.gz truncated/original/U4T/U4T_R1_L8.fastq.gz truncated/original/U4T/U4T_R2_L1.fastq.gz truncated/original/U4T/U4T_R2_L7.fastq.gz truncated/original/U4T/U4T_R2_L8.fastq.gz

.PHONY: truncated_combined
truncated_combined : truncated/combined/TwT1/TwT1_R1.fastq.gz truncated/combined/TwT1/TwT1_R2.fastq.gz truncated/combined/TwT2/TwT2_R1.fastq.gz truncated/combined/TwT2/TwT2_R2.fastq.gz truncated/combined/TwN1/TwN1_R1.fastq.gz truncated/combined/TwN1/TwN1_R2.fastq.gz truncated/combined/TwN2/TwN2_R1.fastq.gz truncated/combined/TwN2/TwN2_R2.fastq.gz truncated/combined/TwT3/TwT3_R1.fastq.gz truncated/combined/TwT3/TwT3_R2.fastq.gz truncated/combined/TwN3/TwN3_R1.fastq.gz truncated/combined/TwN3/TwN3_R2.fastq.gz truncated/combined/U1T/U1T_R1.fastq.gz truncated/combined/U1T/U1T_R2.fastq.gz truncated/combined/U2T/U2T_R1.fastq.gz truncated/combined/U2T/U2T_R2.fastq.gz truncated/combined/U3N/U3N_R1.fastq.gz truncated/combined/U3N/U3N_R2.fastq.gz truncated/combined/U4T/U4T_R1.fastq.gz truncated/combined/U4T/U4T_R2.fastq.gz
