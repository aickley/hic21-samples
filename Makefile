all : samples.mk

files.json : code
	python -m code.search >$@

samples.mk : files.json code
	python -m code.genmakefile $< >$@

.PHONY: groups
groups :
	mkdir -p {twins,unrel}/{production,truncated}/combined
	mkdir -p twins/{production,truncated}/combined/TwT{1,2,3}
	mkdir -p twins/{production,truncated}/combined/TwN{1,2,3}
	mkdir -p unrel/{production,truncated}/combined/U3N
	mkdir -p unrel/{production,truncated}/combined/U{1,2,4}T
	find production/combined -name 'T*.fastq*' | xargs -n 1 -I {} ln {} twins/{}
	find truncated/combined -name 'T*.fastq*'       | xargs -n 1 -I {} ln {} twins/{}
	find production/combined -name 'U*.fastq*' | xargs -n 1 -I {} ln {} unrel/{}
	find truncated/combined -name 'U*.fastq*'       | xargs -n 1 -I {} ln {} unrel/{}
