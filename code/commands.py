from collections import namedtuple
from util import pathstr, wrap_curly


class CopyFile(namedtuple('CopyFile', ['source', 'destination'])):

    cmd = 'cp'

    def __str__(self):
        return ' '.join([self.cmd, pathstr(self.source), pathstr(self.destination)])


class MkDir(namedtuple('MkDir', ['path', 'parents'])):

    @property
    def cmd(self):
        return 'mkdir -p' if self.parents else 'mk'

    def __str__(self):
        return ' '.join([self.cmd, pathstr(self.path)])


class PigzUnpack(namedtuple('PigzUnpack', ['paths'])):

    cmd = 'pigz -dc'

    def __str__(self):
        return ' '.join([self.cmd] + list(map(pathstr, self.paths)))


class PigzPack(namedtuple('PigzPack', [])):

    cmd = 'pigz'

    def __str__(self):
        return self.cmd


class Pipe(object):

    def __init__(self, commands):
        self.commands = commands

    def __str__(self):
        return ' | '.join(map(wrap_curly, map(str, self.commands)))


class Redirect(object):

    def __init__(self, command, stdout=None, stderr=None):
        self.command = command
        self.stdout = pathstr(stdout) if stdout else None
        self.stderr = pathstr(stderr) if stderr else None

    def __str__(self):
        items = []
        items.append(str(self.command))
        if self.stdout:
            items.append('>' + self.stdout)
        if self.stderr:
            items.append('2>' + self.stderr)
        return ' '.join(items)


class Head(namedtuple('Head', ['n_lines'])):

    cmd = 'head'

    @property
    def flags(self):
        return '-n {:d}'.format(self.n_lines)

    def __str__(self):
        return ' '.join([self.cmd, self.flags])

