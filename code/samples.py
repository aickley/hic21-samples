from itertools import groupby

from util import to_json


Metadata = {
    210: dict(genome='Tw', trisomic=1),
    211: dict(genome='Tw', trisomic=1),
    213: dict(genome='Tw', trisomic=0),
    214: dict(genome='Tw', trisomic=0),
    486: dict(genome='Tw', trisomic=1),
    489: dict(genome='Tw', trisomic=0),
    753: dict(genome='U1', trisomic=1), # mislabelled as non-trisomic in archive
    754: dict(genome='U2', trisomic=1),
    755: dict(genome='U3', trisomic=0),
    756: dict(genome='U4', trisomic=1)
}
for num in Metadata:
    Metadata[num]['num'] = num


def _generate_sample_ids():
    samples = list(Metadata.values())
    kf = lambda keys: (lambda sample: tuple((sample[k] for k in keys)))
    samples.sort(key=kf(['genome', 'trisomic', 'num']))
    ids = dict()
    for (g, t), by_src in groupby(samples, key=kf(['genome', 'trisomic'])):
        by_src = list(by_src)
        base_id = g + ('T' if t else 'N')
        if len(by_src) == 1:
            ids[by_src[0]['num']] = base_id
        else:
            replica = 1
            for sample in by_src:
                ids[sample['num']] = base_id + str(replica)
                replica += 1
    return ids

Ids = _generate_sample_ids()


if __name__ == '__main__':
    print(to_json(Ids, sort_keys=True))
