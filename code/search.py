import sys
import itertools

from collections import defaultdict
from pathlib2 import Path
from itertools import groupby, ifilter, imap

import cfg
import samples
from util import to_json, eprint


keyf = lambda keys: (lambda file: tuple((file['meta'][k] for k in keys)))
keyf1 = lambda key: (lambda file: file['meta'][key])

def correct_flag(num, flag):
    'Mislabelled sample'
    return 'T21' if num == 753 else flag

def parse_fastq_stem(stem):
    'Parses a single read FASTQ file stem (e.g. "Sample_SRC")'
    parts = stem.split('_')
    assert parts[-1] == '001'
    parts = parts[:-1]
    num, flag, lane, read = (parts[i] for i in (0, 1, -2, -1))
    assert num[:3] == 'SRC'
    assert lane[0] == 'L'
    assert read[0] == 'R'
    num = int(num[3:])
    lane = lane.replace('00', '')
    flag = correct_flag(num, flag)
    trisomic = flag == 'T21'
    return num, trisomic, lane, read

def file_metadata(stem):
    num, trisomic, lane, read = parse_fastq_stem(stem)
    try:
        sample_meta = samples.Metadata[num]
        sample_id = samples.Ids[num]
    except KeyError:
        return None
    assert trisomic == sample_meta['trisomic']
    meta = dict()
    meta['sample_num'] = num
    meta['lane'] = lane
    meta['read'] = read
    meta['sample_id'] = sample_id
    return meta

def glob(search_path=cfg.Paths['search']):
    targeted = list()
    other = list()
    all_ =  search_path.glob('**/*.fastq.gz')
    with_stem = lambda path: (path, path.stem.rstrip('.fastq'))
    is_src = lambda with_stem_: 'SRC' in with_stem_[1]
    for path, stem in ifilter(is_src, imap(with_stem, all_)):
        meta = file_metadata(stem)
        if meta:
            targeted.append(dict(path=path, meta=meta))
        else:
            other.append(path)
    return targeted, other

def assert_same_size(paths):
    sizes = [p.stat().st_size for p in paths]
    try:
        assert(len(set(sizes))) == 1
        msg = ['Warning: multiple versions (with the same size):']
        msg += ['  ' + str(p) for p in paths]
        msg[1] += ' (kept)'
        eprint('\n'.join(msg))
    except AssertionError:
        paths_info = '\n'.join('{:s}: {:d}'.format(p, s) for p, s in zip(paths, sizes))
        raise AssertionError('Conflicting file sizes: \n{:s}\n'.format(paths_info))

def resolve_meta_duplicates(files):
    'Check that files with same attributes have the same sizes'
    files_ = list()
    keys = ['sample_num', 'read', 'lane']
    files.sort(key=keyf(keys))
    for meta, group in groupby(files, key=keyf(keys)):
        group = list(group)
        paths = [file['path'] for file in group]
        if len(paths) > 1:
            assert_same_size(paths)
            paths = paths[0]
        files_.append(group[0])
    return files_

def check_reads_lanes(files):
    'Check that each lane is present for both reads within a sample'
    files.sort(key=keyf(['sample_num', 'read', 'lane']))
    for num, group in groupby(files, key=keyf1('sample_num')):
        lanes = defaultdict(set)
        group = list(group)
        for read, group2 in groupby(group, key=keyf1('read')):
            lanes[read].update(map(keyf1('lane'), group2))
        if lanes['R1'] != lanes['R2']:
            raise AssertionError('Incoherent reads/lanes for sample {:d}'.format(num))
    return files

def search():
    targeted, _ = glob()
    targeted = resolve_meta_duplicates(targeted)
    targeted = check_reads_lanes(targeted)
    return targeted

if __name__ == '__main__':
    eprint('Searching for samples...')
    targeted = search()

    for file in targeted:
        file['path'] = str(file['path'])

    eprint('Done. Writing JSON to stdout...')
    print(to_json(targeted))
