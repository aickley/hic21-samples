import sys
import itertools
import json

from pathlib2 import Path
from collections import OrderedDict

import cfg
import commands
import util
from rules import Rule, ignore_exit_status, to_makefile


def target_path(meta, keys, prefix):
    stem = '_'.join([meta[k] for k in keys])
    name = stem + '.fastq.gz'
    return prefix / meta['sample_id'] / name

def copy_rule(source, meta, prefix):
    target = target_path(meta, ['sample_id', 'lane', 'read'], prefix)
    actions = [
        commands.MkDir(target.parent, True),
        commands.CopyFile(r'$<', r'$@')
    ]
    # we do not out source in a rule header to be able to
    # use the generated makefile on cluster nodes where
    # `/archive` is unavailable
    return Rule(target, [], actions, meta=dict(meta))

def combine_rule(sources, meta, prefix):
    target = target_path(meta, ['sample_id', 'read'], prefix)
    actions = [
        commands.MkDir(target.parent, True),
        ignore_exit_status(
            commands.Pipe([
                commands.PigzUnpack([r'$^']),
                commands.Redirect(
                    commands.PigzPack(),
                    stdout = target)]))]
    return Rule(target, sources, actions, meta=dict(meta))

def truncate_rule(source, target, lines_to_keep):
    actions = [
        commands.MkDir(target.parent, True),
        ignore_exit_status(
            commands.Pipe([
                commands.PigzUnpack([r'$<']),
                commands.Head(lines_to_keep),
                commands.Redirect(
                    commands.PigzPack(),
                    stdout = r'$@')]))]
    return Rule(target, [source], actions)


if __name__ == '__main__':

    files_with_meta = json.loads(open(sys.argv[1]).read())

    rules = OrderedDict()

    # Copy original files
    outdir = cfg.Paths['production'] / 'original'
    mkrule = lambda f: copy_rule(Path(f['path']), f['meta'], outdir)
    copy_rules = [mkrule(file) for file in files_with_meta]
    rules['original'] = copy_rules

    # Combine all lanes in one file (per sample and read)
    outdir = cfg.Paths['production'] / 'combined'
    keyfunc = lambda rule: (rule.meta['sample_id'], rule.meta['read'])
    combine_rules = []
    for (id, read), grouped_rules in itertools.groupby(copy_rules, keyfunc):
        meta = dict(sample_id=id, read=read)
        sources = [rule.target for rule in grouped_rules]
        combine_rules.append(combine_rule(sources, meta, outdir))
    rules['combined'] = combine_rules

    # Truncated original files
    outdir = cfg.Paths['truncated'] / 'original'
    N = cfg.LinesToKeep
    keys = ['sample_id', 'read', 'lane']
    mkpath = lambda meta: target_path(meta, keys, outdir)
    mkrule = lambda rule: truncate_rule(rule.target, mkpath(rule.meta), N)
    truncate_orig = [mkrule(oldrule) for oldrule in copy_rules]
    rules['truncated_original'] = truncate_orig

    # Truncated combined files
    outdir = cfg.Paths['truncated'] / 'combined'
    N = cfg.LinesToKeep
    keys = ['sample_id', 'read']
    mkpath = lambda meta: target_path(meta, keys, outdir)
    mkrule = lambda rule: truncate_rule(rule.target, mkpath(rule.meta), N)
    truncate_comb = [mkrule(oldrule) for oldrule in combine_rules]
    rules['truncated_combined'] = truncate_comb

    # Truncated original files
    outdir = cfg.Paths['truncated'] / 'original'
    N = cfg.LinesToKeep
    keys = ['sample_id', 'read', 'lane']
    mkpath = lambda meta: target_path(meta, keys, outdir)
    mkrule = lambda rule: truncate_rule(rule.target, mkpath(rule.meta), N)
    truncate_orig = [mkrule(oldrule) for oldrule in copy_rules]
    rules['truncated_original'] = truncate_orig

    # Meta rules
    default = [Rule('all', ['original'], [], phony=True)]  # disable make all

    def make_meta(rules, name):
        inputs = [rule.target for rule in rules]
        actions = []
        return Rule(name, inputs, [], phony=True)

    meta_rules = [make_meta(rules[group], group) for group in rules]

    all_rules = default + list(itertools.chain(*rules.values())) + meta_rules

    print(to_makefile(all_rules))
