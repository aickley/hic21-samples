from pathlib2 import Path

Paths = dict()
Paths['search'] = Path('/archive/ug/unige/medgen/SEAHTS/3C_4C_5C_HiC/')
Paths['production'] = Path('production')
Paths['truncated'] = Path('truncated')

LinesToKeep = 10**6
